#include <windows.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include "../include/Input.hpp"
#include "../include/Timer.hpp"
#include "../include/GameManager.hpp"
#include "glm/glm.hpp"

#include <iostream>
#include <string>
#include <chrono>
#include <ctime>

std::shared_ptr<GameManager> gm;
Timer timer;

int window;

bool keyPressed[30];
int mousePosX, mousePosY;
float moveX, moveY;

void init()
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_DEPTH_TEST);

	// Enable blending
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	timer.restart();

	gm.reset(new GameManager(&timer));
	gm->init();

	for (int i = 0; i < 30; i++)
		keyPressed[i] = false;
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	double dt = timer.frame();

	gm->setFps(timer.fps());
	gm->update(dt);
	gm->render();

	if (keyPressed[KEY_ID_A] == true) {
		gm->moveLeft();
	}
	if (keyPressed[KEY_ID_D] == true) {
		gm->moveRight();
	}
	if (keyPressed[KEY_ID_K]) {
		gm->fireLaser();
	}
	if (keyPressed[KEY_ID_L]) {
		gm->fireBullet();
	}

	glutSwapBuffers();
	glutPostRedisplay();

	GLenum err = glGetError();
	if (err != GL_NO_ERROR)
		std::cout << "OpenGL error: " << gluErrorString(err) << std::endl;
}

void keyDown(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		glutDestroyWindow(window);
#ifndef _WIN32
		// Must use this with regular glut, since it never returns control to main().
		exit(0);
#endif
		break;

	case 'w':
		keyPressed[KEY_ID_W] = true;
		break;
	case 'a':
		keyPressed[KEY_ID_A] = true;
		break;
	case 's':
		keyPressed[KEY_ID_S] = true;
		break;
	case 'd':
		keyPressed[KEY_ID_D] = true;
		break;
	case ' ':
		keyPressed[KEY_ID_SPACE] = true;
		break;
	case 'c':
		keyPressed[KEY_ID_C] = true;
		break;
	case 'h':
		keyPressed[KEY_ID_H] = true;
		break;
	case 'j':
		keyPressed[KEY_ID_J] = true;
		break;
	case 'k':
		keyPressed[KEY_ID_K] = true;
		break;
	case 'l':
		keyPressed[KEY_ID_L] = true;
		break;

	default:
		glutPostRedisplay();
	}

	gm->keyDown(key, x, y);
}

void keyUp(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 'w':
		keyPressed[KEY_ID_W] = false;
		break;
	case 'a':
		keyPressed[KEY_ID_A] = false;
		break;
	case 's':
		keyPressed[KEY_ID_S] = false;
		break;
	case 'd':
		keyPressed[KEY_ID_D] = false;
		break;
	case ' ':
		keyPressed[KEY_ID_SPACE] = false;
		break;
	case 'c':
		keyPressed[KEY_ID_C] = false;
		break;
	case 'h':
		keyPressed[KEY_ID_H] = false;
		break;
	case 'j':
		keyPressed[KEY_ID_J] = false;
		break;
	case 'k':
		keyPressed[KEY_ID_K] = false;
		break;
	case 'l':
		keyPressed[KEY_ID_L] = false;
		break;
	}

	gm->keyUp(key, x, y);
}

void mousePressed(int button, int state, int posX, int posY)
{
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
		mousePosX = posX;
		mousePosY = posY;
		keyPressed[MOUSE_LEFT_BUTTON_DOWN] = true;
		gm->mousePressed((posX / (float)glutGet(GLUT_WINDOW_WIDTH)) * 2 - 1,
			((posY / (float)glutGet(GLUT_WINDOW_HEIGHT)) * 2 - 1) * -1);
	}
	if (button == GLUT_LEFT_BUTTON && state == GLUT_UP)
		keyPressed[MOUSE_LEFT_BUTTON_DOWN] = false;
}

void mouseMoved(int posX, int posY)
{
	if (keyPressed[MOUSE_LEFT_BUTTON_DOWN])
	{
		int diffX = posX - mousePosX;
		mousePosX = posX;
		int diffY = posY - mousePosY;
		mousePosY = posY;

		// Implement quaternion based mouse move
	}
}

void reshape(int w, int h)
{
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
	gm->setProjectionMatrix(glm::perspective(glm::radians(60.0f), float(w) / float(h), 1.0f, 3000.0f));
	//glMatrixMode(GL_PROJECTION);
	//glLoadIdentity();
	//  glOrtho(-50, 700, -50, 700, -50, 50);
	//gluPerspective(60.0f, float(w) / float(h), 1.0f, 3000.0f);

	/*glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(0.0, 100.0, -50.0,
			  0.0, 0.0, 300.0,
			  0.0, 1.0, 0.0);*/
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGB);
	glutInitWindowSize(1400, 700);
	glutInitWindowPosition(10, 10);
	window = glutCreateWindow("Space Shooter 3D");

	GLenum err = glewInit();
	if (err != GLEW_OK) {
		std::cout << "GLEW Error: " << glewGetErrorString(err) << std::endl;
	}

	char* buff = (char*)glGetString(GL_VERSION);
	std::cout << "GL_VERSION: " << buff << std::endl;
	buff = (char*)glGetString(GL_VENDOR);
	std::cout << "GL_VENDOR: " << buff << std::endl;
	buff = (char*)glGetString(GL_RENDERER);
	std::cout << "GL_RENDERER: " << buff << std::endl;

	init();
	glutKeyboardFunc(keyDown); // Key pressed
	glutKeyboardUpFunc(keyUp); // Key released
	glutMouseFunc(mousePressed); // Mouse input
	glutMotionFunc(mouseMoved); // Mouse moved while a mbutton is held

	glutReshapeFunc(reshape); // Window resized
	glutDisplayFunc(display); // Render function

	// Add other callback functions here..

	glutMainLoop();
	return 0;
}
