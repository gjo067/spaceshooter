#version 330 core
out vec4 FragColor;

in vec2 TexCoord;

uniform sampler2D tex;

void main()
{
	vec4 texel = texture(tex, TexCoord);
	if(texel.r + texel.g + texel.b == 0.0) discard;
	FragColor = texel;
}