#version 330 compatibility
out vec4 FragColor;

in vec3 TexCoord;

uniform samplerCube texSkybox;

void main() 
{
	FragColor = texture(texSkybox, TexCoord);
}