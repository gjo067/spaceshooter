#version 330 compatibility
// https://learnopengl.com/Advanced-OpenGL/Cubemaps
out vec4 FragColor;

in vec3 FragPos;
in vec3 Normal;

uniform vec3 eyePos;
uniform samplerCube texSky;

void main()
{
	vec3 I = normalize(FragPos - eyePos);
	vec3 R = reflect(I, normalize(Normal));
	FragColor = vec4(texture(texSky, R).rgb, 0.75);
}