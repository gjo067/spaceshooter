#version 330 core
out vec4 FragColor;

in vec2 TexCoord;

uniform sampler2D texLaser;
uniform vec3 baseColor;

void main()
{
	vec4 bulletTexel = texture(texLaser, TexCoord);
	float alpha = (bulletTexel.r * bulletTexel.g * bulletTexel.b); 
	FragColor = vec4(baseColor, alpha);
}