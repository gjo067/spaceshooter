#version 330 core
// https://learnopengl.com/Lighting/Basic-Lighting
out vec4 FragColor;

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoord;
in vec3 LightPos;

uniform sampler2D texture_diffuse1;
uniform sampler2D texture_diffuse2;
uniform sampler2D texture_specular1;
uniform sampler2D texture_normal1;

uniform vec3 lightColor;
uniform vec3 objectColor;

void main()
{
	float ambientStrength = 0.3f;
	vec3 ambient = ambientStrength * lightColor * vec3(texture(texture_diffuse1, TexCoord));

	vec3 norm = normalize(Normal);
	vec3 lightDir = normalize(LightPos - FragPos);
	float diff = max(dot(norm, lightDir), 0.0f);
	vec3 diffuse = diff * lightColor * vec3(texture(texture_diffuse1, TexCoord));
	vec3 diffuse2 = diff * lightColor * vec3(texture(texture_diffuse2, TexCoord));

	float specularStrength = 0.5;
	vec3 viewDir = normalize(-FragPos);
	vec3 reflectDir = reflect(-lightDir, norm);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0f), 32);
	vec3 specular = specularStrength * spec * lightColor * vec3(texture(texture_specular1, TexCoord));
	
	vec3 result = (ambient + diffuse + diffuse2 + specular);
	FragColor = vec4(result, 1.0f);
}