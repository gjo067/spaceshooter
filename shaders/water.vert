#version 330 compatibility
layout (location = 0) in vec3 aPos;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform float time;
uniform float size;

out vec3 FragPos;
out vec3 Normal;

void main()
{
	float waveFreq = 0.08f;
	float amp = 1.1f;

	// I don't know, looks cool.
	vec3 pos = aPos;
	vec3 a = sin(time + waveFreq * pos) + sin(time + 0.016 + waveFreq * pos);
	pos.y = amp * a.x * a.z;

	gl_Position = projection * view * model * vec4(pos, 1.0f);

	Normal = mat3(transpose(inverse(model))) * gl_Normal;
	FragPos = vec3(model * vec4(pos, 1.0));
}