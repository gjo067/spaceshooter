#pragma once

#include <windows.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "SceneObject.hpp"
#include "Shader.hpp"

class GameManager;
/*
	Model from: https://www.turbosquid.com/FullPreview/Index.cfm/ID/531813
*/
class SpaceShip : public SceneObject
{
public:
	SpaceShip(GameManager* gm);
	~SpaceShip();

	void moveRight(float amount);
	void moveLeft(float amount);

	glm::vec3 getPos() { return pos(); }
	float posX() { return _modelMatrix[3].x; }
	float radius() { return _radius; }

	bool canFireLaser() { return _canFireLaser; }
	bool canFireBullet() { return _canFireBullet; }
	void fireLaser() { _canFireLaser = false; }
	void fireBullet() { _canFireBullet = false; }

	void takeDamage(float damage) { _life -= damage; }

	// Upgrade variables.
	float bulletDamage() { return _bulletDamage; }
	float bulletSpeed() { return _bulletSpeed; }
	float laserDamage() { return _laserDamage; }
	float laserWidth() { return _laserWidth; }
	float life() { return _life; }
	float speed() { return _speed; }

	int upgradeBulletDamage(int gold);
	int upgradeBulletSpeed(int gold);
	int upgradeLaserDamage(int gold);
	int upgradeLaserWidth(int gold);
	int upgradeHealth(int gold);
	int upgradeSpeed(int gold);

	float bulletDamageUpgradeAmount() { return _bulletDamageUpgradeAmount; }
	float bulletSpeedUpgradeAmount() { return _bulletSpeedUpgradeAmount; }
	float laserDamageUpgradeAmount() { return _laserDamageUpgradeAmount; }
	float laserWidthUpgradeAmount() { return _laserWidthUpgradeAmount; }
	float lifeUpgradeAmount() { return _lifeUpgradeAmount; }
	float speedUpgradeAmount() { return _speedUpgradeAmount; }

	int bulletDamageUpgradeCost() { return _bulletDamageUpgradeCost; }
	int bulletSpeedUpgradeCost() { return _bulletSpeedUpgradeCost; }
	int laserDamageUpgradeCost() { return _laserDamageUpgradeCost; }
	int laserWidthUpgradeCost() { return _laserWidthUpgradeCost; }
	int spaceshipLifeUpgradeCost() { return _spaceshipLifeUpgradeCost; }
	int spaceshipSpeedUpgradeCost() { return _spaceshipSpeedUpgradeCost; }

protected:
	void privateInit();
	void privateRender();
	void privateUpdate();

private:
	GameManager* _gm;

	float _radius; // Hitbox radius

	bool _canFireLaser = true;
	float _laserCdTime = 0;
	float _laserCd = 1.5f;
	bool _canFireBullet = true;
	float _bulletCdTime = 0;
	float _bulletCd = 0.6f;

	float _bulletDamage = 1.0f;
	float _bulletSpeed = 450.0f;
	float _laserDamage = 0.35f;
	float _laserWidth = 5.0f;
	float _life = 10.0f;
	float _speed = 100.0f;

	// Variables used for upgrading.
	float _bulletDamageUpgradeAmount = 0.3f;
	float _bulletSpeedUpgradeAmount = 50.0f;
	float _laserDamageUpgradeAmount = 0.1f;
	float _laserWidthUpgradeAmount = 1.0f;
	float _lifeUpgradeAmount = 1.0f;
	float _speedUpgradeAmount = 20.0f;

	int _bulletDamageUpgradeCost = 3;
	int _bulletSpeedUpgradeCost = 3;
	int _laserDamageUpgradeCost = 3;
	int _laserWidthUpgradeCost = 3;
	int _spaceshipLifeUpgradeCost = 3;
	int _spaceshipSpeedUpgradeCost = 3;
};

