#pragma once

#include "SceneObject.hpp"
#include <GL/glew.h>
#include "Shader.hpp"
#include "glm/gtc/type_ptr.hpp"

class GameManager;

/*
	A directional(positional?) light.
	Visible in the scene as a cube, same color as the light.
*/
class DirectionalLight : public SceneObject {
public:
	DirectionalLight(GameManager* gm, glm::vec3 pos, glm::vec3 color);
	~DirectionalLight();

	glm::vec3 color() { return _color; }
	glm::vec3 pos() { return glm::vec3(glm::translate(_modelMatrix, _translation)[3]); }

protected:
	void privateInit();
	void privateRender();
	void privateUpdate();

private:
	GameManager* _gm;
	glm::vec3 _color;
	glm::vec3 _translation;
};