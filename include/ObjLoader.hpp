#pragma once
#define NOMINMAX

#include <string>
#include <vector>
#include "GL/glew.h"
#include "glm/vec3.hpp"
#include "glm/vec2.hpp"
#include <string>
#include "Shader.hpp"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

/*
	Classes and structs to load models using Assimp.
	Code copied from here:
	https://learnopengl.com/Model-Loading/Mesh
	https://learnopengl.com/Model-Loading/Model
*/

struct ModelInfo {
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> texCoords;
	std::vector<unsigned int> triangleIndices;
	std::vector<unsigned int> quadIndices;
};

struct Vertex {
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec2 texCoords;
};

struct Texture {
	unsigned int id;
	std::string type;
	std::string path;
};

class Mesh {
public :
	Mesh(std::vector<Vertex>, std::vector<GLuint>, std::vector<Texture>);
	void draw(Shader* shader);
private:
	GLuint _VAO, _VBO, _EBO;
	std::vector<Vertex> _vertices;
	std::vector<GLuint> _indices;
	std::vector<Texture> _textures;
	void _setupMesh();
};

class Model {
public:
	Model() {}
	void loadModel(std::string path);
	void draw(Shader* shader);

private:
	std::vector<Mesh> _meshes;
	std::string _directory;
	std::vector<Texture> _loaded_textures;

	void _processNode(aiNode* node, const aiScene* scene);
	Mesh _processMesh(aiMesh* mesh, const aiScene* scene);
	std::vector<Texture> _loadMaterialTextures(aiMaterial* mat, aiTextureType type,
		std::string typeName);
};

/*
	Class that loads and stores the two models used in the game.
*/
class ModelProvider {
public:
	ModelProvider();
	~ModelProvider();
	Model* enemy() { return &_enemy; }
	Model* spaceship() { return &_spaceship; }

private:
	Model _enemy;
	Model _spaceship;
};