#pragma once

#include <GL/glew.h>
#include "SceneObject.hpp"

class GameManager;

/*
	Uses geometry shader to create an exploding enemy animation.
*/
class ExplodingEnemy : public SceneObject
{
public:
	ExplodingEnemy(GameManager* gm, glm::mat4 enemyMat, float size, float radius);
	~ExplodingEnemy();

	bool shouldRemove() { return _elapsed > _duration; }

protected:
	void privateInit();
	void privateRender();
	void privateUpdate();

private:
	GameManager* _gm;
	float _elapsed;
	float _duration;
	float _size, _radius;
};