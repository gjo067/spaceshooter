#pragma once

#include <Windows.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "SceneObject.hpp"

#include "Shader.hpp"
#include "../lib/BMPLoader.hpp"

class GameManager;

class Skybox : public SceneObject {
public:
	Skybox(GameManager* gm);
	~Skybox();

	GLuint texture() { return _texture; }

protected:
	void privateInit();
	void privateRender();
	void privateUpdate();

private:
	GLuint _VAO;
	GLuint _EBO;
	GLuint _numVertices;

	GLuint _texture;

	GameManager* _gm;

	Shader _shader;
};