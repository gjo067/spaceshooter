#pragma once

#include <windows.h>
#include <GL/glew.h>
#include "glm/glm.hpp"

/*
	Camera object used to calculate the view matrix.
*/
class Camera
{
public:
	Camera();
	~Camera();

	void moveRight(float amount);
	void moveLeft(float amount);

	bool isMovingRight() { return _movingRight; }
	bool isMovingLeft() { return _movingLeft; }

	glm::mat4 viewMatrix();
	glm::vec3 pos() { return _pos; }

	void keyDown(unsigned char key, int x, int y);
	void keyUp(unsigned char key, int x, int y);

private:
	bool _movingRight = false;
	bool _movingLeft = false;

	glm::vec3 _pos;
	glm::vec3 _lookAt;
	glm::vec3 _up;
};


