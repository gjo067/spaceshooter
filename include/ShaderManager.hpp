#pragma once

#include "Shader.hpp"

/*
	Loads and stores shaders.
*/
class ShaderManager {
public:
	ShaderManager();
	~ShaderManager();

	Shader* spaceshipShader() { return &_spaceshipShader; }
	Shader* enemyShader() { return &_enemyShader; }
	Shader* explodingEnemy() { return &_explodingEnemy; }
	Shader* lightSourceShader() { return &_lightSourceShader; }
	Shader* bulletShader() { return &_bulletShader; }
	Shader* laserShader() { return &_laserShader; }
	Shader* textShader() { return &_textShader; }
	Shader* button() { return &_button; }

private:
	void _loadShaders();

	Shader _spaceshipShader;
	Shader _enemyShader;
	Shader _explodingEnemy;
	Shader _lightSourceShader;
	Shader _bulletShader;
	Shader _laserShader;
	Shader _textShader;
	Shader _button;
};