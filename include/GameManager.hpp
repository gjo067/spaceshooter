#pragma once

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <windows.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <memory>
#include <set>

#include "SceneObject.hpp"
#include "BattleField.hpp"
#include "SpaceShip.hpp"
#include "Camera.hpp"
#include "Enemy.hpp"
#include "Laser.hpp"
#include "Bullet.hpp"
#include "Text.hpp"
#include "Skybox.hpp"
#include "BasicGeometry.hpp"
#include "TextureManager.hpp"
#include "ShaderManager.hpp"
#include "ObjLoader.hpp"
#include "DirectionalLight.hpp"
#include "Water.hpp"
#include "Timer.hpp"
#include "Button.hpp"
#include "ExplodingEnemy.hpp"

class GameManager : public SceneObject
{
public:
	GameManager(Timer* timer);
	~GameManager();

	void keyDown(unsigned char key, int x, int y);
	void keyUp(unsigned char key, int x, int y);
	void mousePressed(float x, float y);
	void moveLeft();
	void moveRight();

	// Getters
	std::shared_ptr<Camera> cam() { return _cam; }
	std::shared_ptr<Skybox> skybox() { return _skybox; }
	std::shared_ptr<DirectionalLight> light() { return _light; }
	std::shared_ptr<SpaceShip> spaceship() { return _spaceship; }
	std::set<std::shared_ptr<Enemy>>& enemies() { return _enemies; }
	BasicGeometry* basicGeom() { return _basicGeom; }
	TextureManager* textures() { return _textureManager; }
	ShaderManager* shaders() { return _shaderManager; }
	ModelProvider* models() { return _modelsProvider; }
	Timer* timer() { return _timer; }
	glm::mat4 projectionMatrix() { return _projectionMatrix; }
	glm::mat4 viewMatrix() { return _viewMatrix; }

	// Setters
	void setProjectionMatrix(glm::mat4 mat) { _projectionMatrix = mat; }
	void setViewMatrix(glm::mat4 mat) { _viewMatrix = mat; }
	void setFps(double fps) { _fps = fps; }

	void removeEnemy(std::shared_ptr<Enemy> enemy);

	void buttonCallback(std::string type);

	// TODO: Move to spaceship
	void fireLaser();
	void fireBullet();

	void addExplodingEnemy(glm::mat4 enemyMatrix, float size, float radius);

	void addScore(int score) { _score += score * 10; _gold += score; }

protected:
	virtual void privateInit();
	virtual void privateRender();
	virtual void privateUpdate();

private:
	std::shared_ptr<BattleField> _bf;
	std::shared_ptr<Water> _water;
	std::shared_ptr<SpaceShip> _spaceship;
	std::set<std::shared_ptr<Enemy>> _enemies;
	std::set<std::shared_ptr<ExplodingEnemy>> _explodingEnemies;
	std::set<std::shared_ptr<Laser>> _lasers;
	std::set<std::shared_ptr<Bullet>> _bullets;
	std::vector<std::shared_ptr<Button>> _buttons;
	std::shared_ptr<Camera> _cam;
	std::shared_ptr<Skybox> _skybox;
	std::shared_ptr<DirectionalLight> _light;
	Text _text;
	BasicGeometry* _basicGeom;
	TextureManager* _textureManager;
	ShaderManager* _shaderManager;
	ModelProvider* _modelsProvider;
	Timer* _timer;

	glm::mat4 _viewMatrix;
	glm::mat4 _projectionMatrix;

	// TODO: Create an enemy spawner class that spawn enemies
	float _enemyCdTime = 3;
	float _lastEnemyTime = 0;
	int _totalEnemiesSpawned = 0;
	int _enemyMaxHealth = 1;

	int _score = 0;
	int _gold = 0;
	double _fps = 0;
};

