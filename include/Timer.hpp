#ifndef _TIMER_H_
#define _TIMER_H_

#ifdef _WIN32
#include <sys/timeb.h>
#include <time.h>
#endif

/** \brief A very basic timer class, suitable for FPS counters etc.
 * From the course website, some edits.
 */
class Timer
{

public:
	Timer() : _startTime(getCurrentTime()), _frames(0), 
		_lastFrameTime(_startTime), _lastFps(0), _elapsedFpsTime(0) {};

	/** Report the elapsed time in seconds (it will return a double,
	 *  so the fractional part is subsecond part). */
	double elapsed() const { return getCurrentTime() - _startTime; };

	/** Increases frame count and return dt of last frame */
	double frame() {
		double current = getCurrentTime();
		double dt = current - _lastFrameTime;
		_frames++;
		_lastFrameTime = current;
		_elapsedFpsTime += dt;
		if (_elapsedFpsTime > 1.0) {
			_lastFps = _frames;
			_frames = 0;
			_elapsedFpsTime = 0;
		}
		return dt;
	}

	double frames() {
		return _frames;
	}

	double fps() {
		return _lastFps;
	}

	/** Restart the timer. */
	void restart() {
		_startTime = getCurrentTime();
		_lastFrameTime = _startTime;
		_frames = 0;
	};

	/** Return the current time as number of elapsed seconds of this day. */
	double static getCurrentTime() {
		struct _timeb ts;
		_ftime64_s(&ts);
		return (ts.time + 0.001*ts.millitm);
	};

private:
	double _startTime;
	double _lastFrameTime;
	double _frames;
	double _lastFps;
	double _elapsedFpsTime;

};
#endif // _TIMER_H_