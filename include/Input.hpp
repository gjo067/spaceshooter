#pragma once

const int KEY_ID_W = 0;
const int KEY_ID_S = 1;
const int KEY_ID_A = 2;
const int KEY_ID_D = 3;
const int KEY_ID_SPACE = 4;
const int KEY_ID_C = 5;
const int KEY_ID_H = 6;
const int KEY_ID_J = 7;
const int KEY_ID_K = 8;
const int KEY_ID_L = 9;

const int MOUSE_LEFT_BUTTON_DOWN = 20;
