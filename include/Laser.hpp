#pragma once

#include <Windows.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "SceneObject.hpp"

class GameManager;

/*
	Laser weapon
	Does a small amount of damage every _tickTime seconds.
*/
class Laser : public SceneObject {
public:
	Laser(GameManager* gm, glm::vec3 startPos, float damage, float width);
	~Laser();

	// Returns true if the laser should be removed from the scene.
	bool shouldDestroy() { return _elapsedTime > _lifespan; }

	// Check if the laser is hitting any enemies.
	void checkForHits();

protected:
	void privateInit();
	void privateRender();
	void privateUpdate();

private:
	float _damage;
	float _length;
	float _width;
	float _lifespan;
	float _tickTime;
	int _totalTicks;
	float _elapsedTime;

	glm::vec3 _startPos;

	GameManager* _gm;
};