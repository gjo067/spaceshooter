#pragma once

#include "GL/glew.h"
#include <string>

class GameManager;

/*
	Basic button
*/
class Button {
public:
	Button(GameManager* gm, float x, float y, float w, float h, std::string callbackType);
	~Button();

	void render();
	void mousePressed(float x, float y);

private:
	GameManager* _gm;
	float _x, _y, _w, _h;
	std::string _callbackType;
};