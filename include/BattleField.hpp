#pragma once

#include <windows.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include "Shader.hpp"
#include "SceneObject.hpp"
#include "glm/glm.hpp"

class GameManager;

/*
	Creates the terrain.
	Height of vertices are taken from heightfield.
*/
class BattleField : public SceneObject
{
public:
	BattleField(GameManager* gm);
	~BattleField();

	float width() { return (_width - 1) * _size; }

protected:
	virtual void privateInit();
	virtual void privateRender();
	virtual void privateUpdate();

private:
	GLuint _VAO;
	GLuint _EBO;
	GLuint _numIndices;

	GLuint _textureHeightMap;
	GLuint _texture;

	const int _width = 32;
	const int _height = 512;
	const float _size = 10.0f;

	GameManager* _gm;

	Shader _shader;
};

