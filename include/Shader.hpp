#ifndef _SHADER_HPP_
#define _SHADER_HPP_

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <windows.h>
#include <GL/glew.h>
#include <vector>
#include <stdlib.h>
#include <stdio.h>
#include <string>


enum ShaderType {V_SHADER, F_SHADER, G_SHADER};

/*
	From the course website.
	Added functionality to use geometry shaders too.
*/
class Shader 
{
	public:
		Shader();
		~Shader();
  
    // enable and disable program
    void enable();
    void disable();
    GLuint getProg();

	void setUniform1i(std::string name, int value);
	void setUniform1f(std::string name, float value);
	void setUniform3fv(std::string name, const GLfloat* valueptr);
	void setUniformMat4fv(std::string name, const GLfloat* valueptr);
    
    bool initShaders(char *fileName, bool useVert, bool useFrag, bool useGeom);


	private:
  
    GLuint vs_, fs_, gs_, prog_;
    GLchar *vertexShaderSource_, *fragmentShaderSource_, *geometryShaderSource_;

    int shaderSize(char *fileName, ShaderType shaderType);
    int readShader(char *fileName, ShaderType shaderType, 
                   char *shaderText, int size);
    int loadShaderSource(const GLchar *vertexShader, const GLchar *fragmentShader);

};

#endif //_SHADER_HPP_

