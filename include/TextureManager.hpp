#pragma once

#include <GL/glew.h>

/*
	Loads and stores textures that are used several times.
*/
class TextureManager {
public:
	TextureManager();
	~TextureManager();

	GLuint bulletTexture() { return _bulletTexture; }
	GLuint laserTexture() { return _laserTexture; }
	GLuint btnShop() { return _btnShop; }

private:
	void _privateInit();

	GLuint _bulletTexture;
	GLuint _laserTexture;
	GLuint _btnShop;
};