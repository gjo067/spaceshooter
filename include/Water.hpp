#pragma once

#include <GL/glew.h>
#include "SceneObject.hpp"
#include "Shader.hpp"

class GameManager;

class Water : public SceneObject {
public:
	Water(GameManager* gm);
	~Water();

protected:
	virtual void privateInit();
	virtual void privateRender();
	virtual void privateUpdate();

private:
	GameManager* _gm;

	GLuint _VAO;
	GLuint _EBO;
	GLuint _numIndices;
	float _size = 1.0f;
	
	Shader _shader;
};