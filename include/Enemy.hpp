#pragma once

#include <Windows.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "SceneObject.hpp"
#include "Shader.hpp"
#include "ObjLoader.hpp"

class GameManager;

/*
	Basic enemy
	Size, speed and color based on health
	Can shoot
	Model from: https://www.turbosquid.com/FullPreview/Index.cfm/ID/1081073
*/
class Enemy : public SceneObject
{
public:
	Enemy(GameManager* gm, float health, float strafeWidth, glm::vec3 pos);
	~Enemy();

	void setPosition(glm::vec3);
	glm::vec3 getPos() { return pos(); }

	float size() { return _size; }
	float radius() { return _radius; }

	bool canShoot() { return _canFireBullet; }
	void fireBullet() { _canFireBullet = false; }

	bool takeDamage(float damage);
	float maxHealth() { return _startHealth; }
	float health() { return _health; }

protected:
	void privateInit();
	void privateRender();
	void privateUpdate();

private:
	float _startHealth;
	float _health;
	float _strafeCounter;
	float _size;
	float _speed;
	float _strafeWidth;
	float _radius;

	GameManager* _gm;

	bool _canFireBullet = false;
	double _bulletCdTime = 0;
	double _bulletCd = 3;

	glm::vec3 _getColorFromHealth();
};