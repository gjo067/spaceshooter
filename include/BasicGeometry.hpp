#pragma once

#include <GL/glew.h>

/* 
	Creates some basic geometry for other objects to use.
	All objects are unit sized.
*/

class BasicGeometry {
public:
	BasicGeometry();
	~BasicGeometry();

	GLuint cubeVAO() { return _cubeVAO; }
	int numCubeVertices() { return _numCubeVertices; }

	GLuint textureQuadVAO() { return _texturedQuadVAO; }
	int numTexturedQuadVertices() { return _numTexturedQuadVertices; }

private:
	void _privateInit();

	GLuint _cubeVAO;
	int _numCubeVertices;

	GLuint _texturedQuadVAO;
	int _numTexturedQuadVertices;
};