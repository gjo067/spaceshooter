#pragma once

#include <Windows.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "SceneObject.hpp"
#include "Shader.hpp"

class GameManager;

class Bullet : public SceneObject {
public:
	Bullet(GameManager* gm, glm::vec3 startPos, glm::vec3 dir, bool fromPlayer, float damage, float speed);
	~Bullet();

	// Check if the bullet can be removed from the scene.
	bool shouldDestroy();

	// Checks if the bullet has hit a target
	bool checkForHits();

protected:
	void privateInit();
	void privateRender();
	void privateUpdate();

private:
	float _damage;
	float _speed;
	double _lifespan;
	double _elapsedTime;
	bool _fromPlayer; // From player or enemy
	glm::vec3 _direction;
	GameManager* _gm;
	float _size;
};