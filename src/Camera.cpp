#include "../include/Camera.hpp"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

Camera::Camera() :
	_pos(glm::vec3(0.0f, 50.0f, -100.0f)),
	_lookAt(glm::vec3(0.0f, 0.0f, 200.0f)),
	_up(glm::vec3(0.0f, 1.0f, 0.0f))
{
}

Camera::~Camera()
{
}

void Camera::moveRight(float amount)
{
	_movingRight = true;
	_pos.x += amount;
	_lookAt.x += amount;
}

void Camera::moveLeft(float amount)
{
	_movingLeft = true;
	_pos.x += amount;
	_lookAt.x += amount;
}

glm::mat4 Camera::viewMatrix()
{
	return glm::lookAt(
		_pos,
		_lookAt,
		_up
	);
}

void Camera::keyDown(unsigned char key, int x, int y)
{
}

void Camera::keyUp(unsigned char key, int x, int y)
{
	switch (key) {
	case 'd':
		if (_movingRight) {
			_movingRight = false;
		}
		break;
	case 'a':
		if (_movingLeft) {
			_movingLeft = false;
		}
		break;
	}
}

