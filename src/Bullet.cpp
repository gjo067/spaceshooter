#include "../include/Bullet.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "../include/GameManager.hpp"

Bullet::Bullet(GameManager* gm, glm::vec3 startPos, glm::vec3 direction, bool fromPlayer, float damage, float speed)
	: _gm(gm), _direction(direction), _fromPlayer(fromPlayer),
	_lifespan(15.0), _elapsedTime(0.0), _size(3.0f), _speed(speed), _damage(damage)
{
	_modelMatrix[3] = glm::vec4(startPos, _modelMatrix[3][3]);
}

Bullet::~Bullet()
{
}

bool Bullet::shouldDestroy()
{
	if (_elapsedTime > _lifespan) {
		return true;
	}
	else if (_modelMatrix[3].z < -300 || _modelMatrix[3].z > 5120) {
		return true;
	}
	else {
		return false;
	}
}

void Bullet::privateInit()
{
}

void Bullet::privateRender()
{
	_elapsedTime += _dt;

	_modelMatrix = glm::translate(_modelMatrix, glm::vec3(_direction[0] * _speed * _dt,
		_direction[1] * _speed * _dt,
		_direction[2] * _speed * _dt));

	auto shader = _gm->shaders()->bulletShader();

	shader->enable();

	shader->setUniformMat4fv("model", glm::value_ptr(_modelMatrix));
	shader->setUniformMat4fv("projection", glm::value_ptr(_gm->projectionMatrix()));
	shader->setUniformMat4fv("view", glm::value_ptr(_gm->viewMatrix()));
	shader->setUniform1i("texBullet", 0);
	shader->setUniform1f("size", _size);
	glm::vec3 color = glm::vec3(0.0f, 0.0f, 0.0f);
	if (_fromPlayer)
		color.b = 1.0f;
	else
		color.r = 1.0f;
	shader->setUniform3fv("baseColor", glm::value_ptr(color));

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, _gm->textures()->bulletTexture());
	
	glBindVertexArray(_gm->basicGeom()->textureQuadVAO());
	glDrawArrays(GL_QUADS, 0, _gm->basicGeom()->numTexturedQuadVertices());
	glBindVertexArray(0);

	shader->disable();

	glBindTexture(GL_TEXTURE_2D, 0);
}

void Bullet::privateUpdate()
{
}

bool Bullet::checkForHits() {
	std::set<std::shared_ptr<Enemy>> enemies = _gm->enemies();
	glm::vec3 bulletPos = { _modelMatrix[3].x, _modelMatrix[3].y, _modelMatrix[3].z };

	if (_fromPlayer) {
		for (auto ei = enemies.begin(); ei != enemies.end(); ) {
			auto pos = (*ei)->getPos();
			auto radius = (*ei)->radius();

			if (glm::length(bulletPos - pos) <= radius + _size / 2) {
				printf("--Bullet hit enemy\n");
				if ((*ei)->takeDamage(_damage)) {
					_gm->addExplodingEnemy((*ei)->getModelMatrix(), (*ei)->size(), (*ei)->radius());
					_gm->addScore((int)(*ei)->maxHealth());
					_gm->removeEnemy((*ei));
				}
				return true;
			}
			else {
				++ei;
			}
		}
	}
	else {
		auto player = _gm->spaceship();
		glm::vec3 bulletPos = { _modelMatrix[3].x, _modelMatrix[3].y, _modelMatrix[3].z };

		if (glm::length(bulletPos - player->getPos()) <= player->radius() + _size / 2) {
			printf("--Bullet hit player\n");
			player->takeDamage(1.0f);
			return true;
		}
	}

	return false;
}
