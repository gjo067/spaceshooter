
#include "../include/SpaceShip.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "../include/GameManager.hpp"
#include <iostream>

SpaceShip::SpaceShip(GameManager* gm) : _gm(gm)
{
	_radius = 12.0;
}

SpaceShip::~SpaceShip()
{
}

void SpaceShip::moveRight(float amount) {
	_modelMatrix = glm::translate(_modelMatrix, glm::vec3(amount, 0.0f, 0.0f));
}

void SpaceShip::moveLeft(float amount) {
	_modelMatrix = glm::translate(_modelMatrix, glm::vec3(amount, 0.0f, 0.0f));
}

void SpaceShip::privateInit()
{
}

void SpaceShip::privateRender()
{

	glm::mat4 model = _modelMatrix;
	model = glm::rotate(model, glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	if (_gm->cam()->isMovingLeft()) {
		model = glm::rotate(model, glm::radians(15.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	}
	else if (_gm->cam()->isMovingRight()) {
		model = glm::rotate(model, glm::radians(-15.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	}
	model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));

	Shader* shader = _gm->shaders()->spaceshipShader();
	shader->enable();

	shader->setUniform3fv("lightColor", glm::value_ptr(_gm->light()->color()));
	shader->setUniform3fv("objectColor", glm::value_ptr(glm::vec3(0.7f, 0.0f, 1.0f)));
	shader->setUniform3fv("lightPos", glm::value_ptr(_gm->light()->pos()));
	shader->setUniformMat4fv("model", glm::value_ptr(model));
	shader->setUniformMat4fv("projection", glm::value_ptr(_gm->projectionMatrix()));
	shader->setUniformMat4fv("view", glm::value_ptr(_gm->viewMatrix()));

	_gm->models()->spaceship()->draw(shader);

	shader->disable();
}

void SpaceShip::privateUpdate()
{
	// Check if any of the weapons are on cooldown, if yes check if they are ready to come off cooldown.
	if (!_canFireLaser) {
		_laserCdTime += (float)_dt;
		if (_laserCdTime >= _laserCd) {
			_laserCdTime = 0;
			_canFireLaser = true;
		}
	}

	if (!_canFireBullet) {
		_bulletCdTime += (float)_dt;
		if (_bulletCdTime >= _bulletCd) {
			_bulletCdTime = 0;
			_canFireBullet = true;
		}
	}
}

int SpaceShip::upgradeBulletDamage(int gold)
{
	int spentGold = 0;
	if (gold >= _bulletDamageUpgradeCost) {
		_bulletDamage += _bulletDamageUpgradeAmount;
		spentGold = _bulletDamageUpgradeCost;
		_bulletDamageUpgradeCost++;
		std::cout << "--bullet damage upgraded" << std::endl;
	}
	return spentGold;
}

int SpaceShip::upgradeBulletSpeed(int gold)
{
	int spentGold = 0;
	if (gold >= _bulletSpeedUpgradeCost) {
		_bulletSpeed += _bulletSpeedUpgradeAmount;
		spentGold = _bulletSpeedUpgradeCost;
		_bulletSpeedUpgradeCost++;
		std::cout << "--bullet speed upgraded" << std::endl;
	}
	return spentGold;
}

int SpaceShip::upgradeLaserDamage(int gold)
{
	int spentGold = 0;
	if (gold >= _laserDamageUpgradeCost) {
		_laserDamage += _laserDamageUpgradeAmount;
		spentGold = _laserDamageUpgradeCost;
		_laserDamageUpgradeCost++;
		std::cout << "--laser damage upgraded" << std::endl;
	}
	return spentGold;
}

int SpaceShip::upgradeLaserWidth(int gold)
{
	int spentGold = 0;
	if (gold >= _laserWidthUpgradeCost) {
		_laserWidth += _laserWidthUpgradeAmount;
		spentGold = _laserWidthUpgradeCost;
		_laserWidthUpgradeCost++;
		std::cout << "--laser width upgraded" << std::endl;
	}
	return spentGold;
}

int SpaceShip::upgradeHealth(int gold)
{
	int spentGold = 0;
	if (gold >= _spaceshipLifeUpgradeCost) {
		_life += _lifeUpgradeAmount;
		spentGold = _spaceshipLifeUpgradeCost;
		std::cout << "--spaceship health upgraded" << std::endl;
	}
	return spentGold;
}

int SpaceShip::upgradeSpeed(int gold)
{
	int spentGold = 0;
	if (gold >= _spaceshipSpeedUpgradeCost) {
		_speed += _speedUpgradeAmount;
		spentGold = _spaceshipSpeedUpgradeCost;
		_spaceshipSpeedUpgradeCost++;
		std::cout << "--spaceship speed upgraded" << std::endl;
	}
	return spentGold;
}

