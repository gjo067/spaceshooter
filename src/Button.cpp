#include "..\include\Button.hpp"
#include "../include/GameManager.hpp"

Button::Button(GameManager * gm, float x, float y, float w, float h, std::string callbackType) 
	: _gm(gm), _x(x), _y(y), _w(w), _h(h), _callbackType(callbackType)
{
}

Button::~Button()
{
}

void Button::render()
{
	glm::mat4 model(1.0f);
	model = glm::translate(model, glm::vec3(_x, _y, 0.0f));
	model = glm::scale(model, glm::vec3(_w, _h, 1.0f));

	auto shader = _gm->shaders()->button();

	shader->enable();

	shader->setUniformMat4fv("model", glm::value_ptr(model));
	shader->setUniform1i("tex", 3);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, _gm->textures()->btnShop());

	glBindVertexArray(_gm->basicGeom()->textureQuadVAO());
	glDrawArrays(GL_QUADS, 0, _gm->basicGeom()->numTexturedQuadVertices());
	glBindVertexArray(0);

	glBindTexture(GL_TEXTURE_2D, 0);

	shader->disable();
}

void Button::mousePressed(float x, float y)
{
	if (std::abs(x - _x) < _w / 2 && std::abs(y - _y) < _h / 2) {
		_gm->buttonCallback(_callbackType);
	}
}
