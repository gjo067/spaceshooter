#include "..\include\DirectionalLight.hpp"
#include "../include/GameManager.hpp"

// TODO: Actually a positional light?

DirectionalLight::DirectionalLight(GameManager * gm, glm::vec3 pos, glm::vec3 color)
	: _gm(gm), _color(color)
{
	_modelMatrix[3] = glm::vec4(pos, _modelMatrix[3][3]);
}

DirectionalLight::~DirectionalLight()
{
}

void DirectionalLight::privateInit()
{
}

void DirectionalLight::privateRender()
{
	Shader* shader = _gm->shaders()->lightSourceShader();
	shader->enable();

	double time = (_gm->timer()->elapsed()) * 0.5;
	_translation = glm::vec3(std::cos(time) * 170, 0, std::sin(time) * 320);
	glm::mat4 model = glm::translate(_modelMatrix, _translation);

	shader->setUniformMat4fv("model", glm::value_ptr(model));
	shader->setUniformMat4fv("projection", glm::value_ptr(_gm->projectionMatrix()));
	shader->setUniformMat4fv("view", glm::value_ptr(_gm->viewMatrix()));
	shader->setUniform3fv("lightColor", glm::value_ptr(_color));
	shader->setUniform1f("size", 10.0f);

	glBindVertexArray(_gm->basicGeom()->cubeVAO());
	glDrawArrays(GL_QUADS, 0, _gm->basicGeom()->numCubeVertices());
	glBindVertexArray(0);

	shader->disable();
}

void DirectionalLight::privateUpdate()
{
}
