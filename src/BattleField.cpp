#include "../include/BattleField.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "../lib/BMPLoader.hpp"
#include "../include/GameManager.hpp"

BattleField::BattleField(GameManager* gm) : _gm(gm)
{
}

BattleField::~BattleField()
{
	glDeleteVertexArrays(1, &_VAO);
}

void BattleField::privateInit()
{
	BMPClass bmp;

	glGenTextures(1, &_textureHeightMap);
	glBindTexture(GL_TEXTURE_2D, _textureHeightMap);

	BMPLoad("img/heightMap2012.bmp", bmp);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bmp.width, bmp.height, 0, GL_RGB, GL_UNSIGNED_BYTE, bmp.bytes);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> texCoords;
	std::vector<unsigned int> indices;

	for (int i = 0; i < _height; ++i) {
		for (int j = 0; j < _width; ++j) {
			vertices.push_back(glm::vec3(j * _size, 0.0f, i * _size));
			texCoords.push_back(glm::vec2(j / (_width * 1.0f), i / (_height * 1.0f)));
		}
	}

	// Set height of vertice from heightmap
	for (unsigned int i = 0; i < vertices.size(); ++i) {
		int vertexRow = i / _width;
		int vertexCol = i % _width;
		int bmpCol = vertexCol * (int)std::floor(bmp.width / _width);
		int bmpRow = vertexRow * (int)std::floor(bmp.height / _height);
		int index = bmpRow * bmp.width * 3 + bmpCol * 3;
		float height = ((bmp.bytes[index] * bmp.bytes[index] * bmp.bytes[index]) / (float)std::pow(255, 3)) * 50.0f;
		vertices[i].y = height;
	}

	// Calculate normals
	for (int i = 0; i < _height; ++i) {
		for (int j = 0; j < _width; ++j) {
			glm::vec3 n{ 0.0f, 0.0f, 0.0f };
			glm::vec3 left, up, down, right;
			bool isLeft = false, isUp = false, isDown = false, isRight = false;
			int cur = i * _width + j;

			if (j < (_width - 1)) {
				left = vertices[cur + 1] - vertices[cur];
				isLeft = true;
			}
			if (j > 0) {
				right = vertices[cur - 1] - vertices[cur];
				isRight = true;
			}
			if (i < (_height - 1)) {
				up = vertices[cur + _width] - vertices[cur];
				isUp = true;
			}
			if (i > 0) {
				down = vertices[cur - _width] - vertices[cur];
				isDown = true;
			}

			if (isUp && isLeft) {
				n += glm::cross(up, left);
			}
			if (isLeft && isDown) {
				n += glm::cross(left, down);
			}
			if (isDown && isRight) {
				n += glm::cross(down, right);
			}
			if (isRight && isUp) {
				n += glm::cross(right, up);
			}

			normals.push_back(glm::normalize(n));
		}
	}

	for (int i = 0; i < _height - 1; ++i) { // i = [0,511]
		for (int j = 0; j < _width; ++j) { // j = [0,31]
			// 0, 32, 1, 33, 2, 34, 3, 35, ... , 31, 63, 63, 32
			// 32, 64, 33, 65, 34, 66, 35, 67, ... , 63, 95, 95, 64
			indices.push_back(j + i * _width);
			indices.push_back(j + _width + i * _width);
		}
		if (i < _height - 1) {
			indices.push_back((i + 2) * _width - 1);
			indices.push_back((i + 1) * _width);
		}
	}

	/*for (int i = 0; i < (_height - 1); ++i) {
		for (int j = 0; j < (_width - 1); ++j) {
			indices.push_back(i * _width + j);
			indices.push_back(i * _width + j + 1);
			indices.push_back((i + 1) * _width + j + 1);
			indices.push_back((i + 1) * _width + j);
		}
	}*/

	_numIndices = indices.size();

	GLuint VBO, NBO, TBO;
	glGenVertexArrays(1, &_VAO);
	glGenBuffers(1, &_EBO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &NBO);
	glGenBuffers(1, &TBO);
	glBindVertexArray(_VAO);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * indices.size(), &indices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * vertices.size(), &vertices[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (void*)0);

	glBindBuffer(GL_ARRAY_BUFFER, NBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * normals.size(), &normals[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (void*)0);

	glBindBuffer(GL_ARRAY_BUFFER, TBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * texCoords.size(), &texCoords[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), (void*)0);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	_modelMatrix = glm::translate(_modelMatrix, glm::vec3(-((_width - 1) * _size) / 2, -100, (_width * _size) / 2 - 200));
	
	glGenTextures(1, &_texture);
	glBindTexture(GL_TEXTURE_2D, _texture);

	BMPLoad("img/colorMap2012.bmp", bmp);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bmp.width, bmp.height, 0, GL_RGB, GL_UNSIGNED_BYTE, bmp.bytes);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glBindTexture(GL_TEXTURE_2D, 0);

	std::string name = "shaders/landscape";
	_shader.initShaders(const_cast<char*>(name.c_str()), true, true, false);

	_shader.enable();
	_shader.setUniform1i("texColor", 0);
	_shader.disable();
}

void BattleField::privateRender()
{
	_shader.enable();

	_shader.setUniformMat4fv("model", glm::value_ptr(_modelMatrix));
	_shader.setUniformMat4fv("view", glm::value_ptr(_gm->viewMatrix()));
	_shader.setUniformMat4fv("projection", glm::value_ptr(_gm->projectionMatrix()));
	_shader.setUniform3fv("lightPos", glm::value_ptr(_gm->light()->pos()));
	_shader.setUniform3fv("lightColor", glm::value_ptr(_gm->light()->color()));

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, _texture);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, _textureHeightMap);

	glBindVertexArray(_VAO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _EBO);
	glDrawElements(GL_TRIANGLE_STRIP, _numIndices, GL_UNSIGNED_INT, 0);

	glBindVertexArray(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindTexture(GL_TEXTURE_2D, 0);

	_shader.disable();
}

void BattleField::privateUpdate()
{
}

