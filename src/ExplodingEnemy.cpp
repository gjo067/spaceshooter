#include "..\include\ExplodingEnemy.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "../include/GameManager.hpp"

ExplodingEnemy::ExplodingEnemy(GameManager * gm, glm::mat4 enemyMat, float size, float radius)
	: _gm(gm), _elapsed(0.0), _duration(1.0), _size(size), _radius(radius)
{
	_modelMatrix = enemyMat;
}

ExplodingEnemy::~ExplodingEnemy()
{
}

void ExplodingEnemy::privateInit()
{
}

void ExplodingEnemy::privateRender()
{
	glm::mat4 model = _modelMatrix;
	model = glm::translate(model, glm::vec3(0.0f, -_radius * 0.67f, 0.0f));
	model = glm::rotate(model, (float)glm::radians(_dt * 10.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(_size, _size, _size));

	Shader* shader = _gm->shaders()->explodingEnemy();
	shader->enable();

	shader->setUniform1f("size", _size);
	shader->setUniform3fv("lightColor", glm::value_ptr(_gm->light()->color()));
	shader->setUniform3fv("objectColor", glm::value_ptr(glm::vec3(1.0f, 0.0f, 0.0f)));
	shader->setUniform3fv("lightPos", glm::value_ptr(_gm->light()->pos()));
	shader->setUniformMat4fv("model", glm::value_ptr(model));
	shader->setUniformMat4fv("projection", glm::value_ptr(_gm->projectionMatrix()));
	shader->setUniformMat4fv("view", glm::value_ptr(_gm->viewMatrix()));
	shader->setUniform1f("time", _elapsed);

	_gm->models()->enemy()->draw(shader);

	shader->disable();
}

void ExplodingEnemy::privateUpdate()
{
	_elapsed += (float)_dt;
}
