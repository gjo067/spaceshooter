#include "../include/SceneObject.hpp"

#include <windows.h>
#include <GL/gl.h>
#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"

SceneObject::SceneObject()
{
	_modelMatrix = glm::mat4(1.0);
}

SceneObject::~SceneObject()
{
}

void SceneObject::render()
{
	this->privateRender();
	for (std::vector<std::shared_ptr<SceneObject> >::iterator it = _children.begin();
		it != _children.end(); it++)
		(*it)->render();
}

void SceneObject::update(double fps)
{
	this->_dt = fps;
	this->privateUpdate();
	for (std::vector<std::shared_ptr<SceneObject> >::iterator it = _children.begin();
		it != _children.end(); it++)
		(*it)->update(fps);
}

void SceneObject::init()
{
	this->privateInit();
	for (std::vector<std::shared_ptr<SceneObject> >::iterator it = _children.begin();
		it != _children.end(); it++)
		(*it)->init();
}

void SceneObject::addSubObject(std::shared_ptr<SceneObject> newchild)
{
	_children.push_back(newchild);
}

void SceneObject::removeSubObject(const std::shared_ptr<SceneObject> child)
{
	for (std::vector<std::shared_ptr<SceneObject> >::iterator it = _children.begin();
		it != _children.end(); it++)
		if (*it == child)
		{
			_children.erase(it);
			break;
		}
}


