#include "../include/Laser.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "../include/GameManager.hpp"

Laser::Laser(GameManager* gm, glm::vec3 startPos, float damage, float width)
	: _gm(gm), _startPos(startPos), _length(800.0f), _lifespan(0.6f),
	_elapsedTime(0.0f), _width(width), _damage(damage), _tickTime(0.15f), _totalTicks(0)
{
	checkForHits();
}

Laser::~Laser()
{
}

void Laser::privateInit()
{
}

void Laser::privateRender()
{
	_elapsedTime += (float)_dt;

	auto shader = _gm->shaders()->laserShader();

	shader->enable();

	glm::vec3 color(0.7f, 0.0f, 0.7f);

	glm::mat4 model = _gm->spaceship()->getModelMatrix();

	glm::vec3 translation(0.0f, 0.0f, _length / 2);
	glm::vec3 rotation(1.0f, 0.0f, 0.0f);
	glm::vec3 scale(_width, _length, 0.0f);

	model = glm::translate(model, translation);
	model = glm::rotate(model, glm::radians(-90.0f), rotation);
	model = glm::scale(model, scale);

	shader->setUniformMat4fv("model", glm::value_ptr(model));
	shader->setUniformMat4fv("view", glm::value_ptr(_gm->viewMatrix()));
	shader->setUniformMat4fv("projection", glm::value_ptr(_gm->projectionMatrix()));
	shader->setUniform1i("texLaser", 0);
	shader->setUniform3fv("baseColor", glm::value_ptr(color));

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, _gm->textures()->laserTexture());

	glBindVertexArray(_gm->basicGeom()->textureQuadVAO());
	glDrawArrays(GL_QUADS, 0, _gm->basicGeom()->numTexturedQuadVertices());
	glBindVertexArray(0);

	shader->disable();

	glBindTexture(GL_TEXTURE_2D, 0);
}

void Laser::privateUpdate()
{
}

void Laser::checkForHits() {
	if (_elapsedTime > _totalTicks * _tickTime) {
		_totalTicks++;

		std::set<std::shared_ptr<Enemy>> enemies = _gm->enemies();

		for (auto ei = enemies.begin(); ei != enemies.end(); ) {
			auto pos = (*ei)->getPos();
			auto radius = (*ei)->radius();
			if (_startPos.x + _width / 2 >= pos.x - radius && _startPos.x - _width / 2 <= pos.x + radius) {
				printf("--Laser hit enemy\n");
				auto enemy = (*ei);
				++ei;
				if (enemy->takeDamage(_damage)) {
					_gm->addExplodingEnemy(enemy->getModelMatrix(), enemy->size(), enemy->radius());
					_gm->addScore((int)enemy->maxHealth());
					_gm->removeEnemy(enemy);
				}
			}
			else {
				++ei;
			}
		}
	}
}
