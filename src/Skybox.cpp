#include "../include/Skybox.hpp"
#include "glm/gtc/type_ptr.hpp" 
#include "../include/GameManager.hpp"

Skybox::Skybox(GameManager* gm) : _gm(gm) {
	_modelMatrix = glm::translate(_modelMatrix, glm::vec3(0.0f, -250.0f, 20.0f));
}

Skybox::~Skybox() {
	glDeleteVertexArrays(1, &_VAO);
}

void Skybox::privateInit() {
	float halfWidth = 1000.0f;
	std::vector<glm::vec3> vertices = {
		glm::vec3(-halfWidth, -halfWidth, halfWidth),
		glm::vec3(-halfWidth, -halfWidth, -halfWidth),
		glm::vec3(-halfWidth, halfWidth, -halfWidth),
		glm::vec3(-halfWidth, halfWidth, halfWidth),

		glm::vec3(halfWidth, -halfWidth, -halfWidth),
		glm::vec3(halfWidth, -halfWidth, halfWidth),
		glm::vec3(halfWidth, halfWidth, halfWidth),
		glm::vec3(halfWidth, halfWidth, -halfWidth),

		glm::vec3(-halfWidth, -halfWidth, -halfWidth),
		glm::vec3(halfWidth, -halfWidth, -halfWidth),
		glm::vec3(halfWidth, halfWidth, -halfWidth),
		glm::vec3(-halfWidth, halfWidth, -halfWidth),

		glm::vec3(halfWidth, -halfWidth, halfWidth),
		glm::vec3(-halfWidth, -halfWidth, halfWidth),
		glm::vec3(-halfWidth, halfWidth, halfWidth),
		glm::vec3(halfWidth, halfWidth, halfWidth),

		glm::vec3(-halfWidth, halfWidth, halfWidth),
		glm::vec3(-halfWidth, halfWidth, -halfWidth),
		glm::vec3(halfWidth, halfWidth, -halfWidth),
		glm::vec3(halfWidth, halfWidth, halfWidth),

		glm::vec3(-halfWidth, -halfWidth, -halfWidth),
		glm::vec3(-halfWidth, -halfWidth, halfWidth),
		glm::vec3(halfWidth, -halfWidth, halfWidth),
		glm::vec3(halfWidth, -halfWidth, -halfWidth)
	};
	_numVertices = vertices.size();

	std::vector<glm::vec3> texCoords = {
		glm::vec3(1.0f, 1.0f, -1.0f),
		glm::vec3(1.0f, 1.0f, 1.0f),
		glm::vec3(1.0f, -1.0f, 1.0f),
		glm::vec3(1.0f, -1.0f, -1.0f),

		glm::vec3(-1.0f, 1.0f, 1.0f),
		glm::vec3(-1.0f, 1.0f, -1.0f),
		glm::vec3(-1.0f, -1.0f, -1.0f),
		glm::vec3(-1.0f, -1.0f, 1.0f),

		glm::vec3(1.0f, 1.0f, 1.0f),
		glm::vec3(-1.0f, 1.0f, 1.0f),
		glm::vec3(-1.0f, -1.0f, 1.0f),
		glm::vec3(1.0f, -1.0f, 1.0f),

		glm::vec3(-1.0f, 1.0f, -1.0f),
		glm::vec3(1.0f, 1.0f, -1.0f),
		glm::vec3(1.0f, -1.0f, -1.0f),
		glm::vec3(-1.0f, -1.0f, -1.0f),

		glm::vec3(1.0f, -1.0f, -1.0f),
		glm::vec3(1.0f, -1.0f, 1.0f),
		glm::vec3(-1.0f, -1.0f, 1.0f),
		glm::vec3(-1.0f, -1.0f, -1.0f),

		glm::vec3(1.0f, 1.0f, 1.0f),
		glm::vec3(1.0f, 1.0f, -1.0f),
		glm::vec3(-1.0f, 1.0f, -1.0f),
		glm::vec3(-1.0f, 1.0f, 1.0f)
	};

	GLuint VBO, TBO;
	glGenVertexArrays(1, &_VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &TBO);
	glBindVertexArray(_VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * vertices.size(), &vertices[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (void*)0);

	glBindBuffer(GL_ARRAY_BUFFER, TBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * texCoords.size(), &texCoords[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (void*)0);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenTextures(1, &_texture);
	glBindTexture(GL_TEXTURE_CUBE_MAP, _texture);

	BMPClass bmp;

	BMPLoad("img/skybox_west.bmp", bmp);
	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGB, bmp.width, bmp.height, 0, GL_RGB, GL_UNSIGNED_BYTE, bmp.bytes);

	BMPLoad("img/skybox_east.bmp", bmp);
	glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGB, bmp.width, bmp.height, 0, GL_RGB, GL_UNSIGNED_BYTE, bmp.bytes);

	BMPLoad("img/skybox_down.bmp", bmp);
	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGB, bmp.width, bmp.height, 0, GL_RGB, GL_UNSIGNED_BYTE, bmp.bytes);

	BMPLoad("img/skybox_up.bmp", bmp);
	glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGB, bmp.width, bmp.height, 0, GL_RGB, GL_UNSIGNED_BYTE, bmp.bytes);

	BMPLoad("img/skybox_south.bmp", bmp);
	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGB, bmp.width, bmp.height, 0, GL_RGB, GL_UNSIGNED_BYTE, bmp.bytes);

	BMPLoad("img/skybox_north.bmp", bmp);
	glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGB, bmp.width, bmp.height, 0, GL_RGB, GL_UNSIGNED_BYTE, bmp.bytes);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	std::string name = "shaders/skybox";
	_shader.initShaders(const_cast<char*>(name.c_str()), true, true, false);
}

void Skybox::privateRender() {
	_shader.enable();

	_shader.setUniformMat4fv("model", glm::value_ptr(_modelMatrix));
	_shader.setUniformMat4fv("projection", glm::value_ptr(_gm->projectionMatrix()));
	_shader.setUniformMat4fv("view", glm::value_ptr(glm::mat4(glm::mat3(_gm->viewMatrix()))));
	_shader.setUniform1i("texSkybox", 8);

	glDisable(GL_DEPTH_TEST);
	glActiveTexture(GL_TEXTURE8);
	glBindTexture(GL_TEXTURE_CUBE_MAP, _texture);

	glBindVertexArray(_VAO);
	glDrawArrays(GL_QUADS, 0, _numVertices);
	glBindVertexArray(0);

	glEnable(GL_DEPTH_TEST);

	_shader.disable();
}

void Skybox::privateUpdate() {}