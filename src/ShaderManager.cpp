#include "..\include\ShaderManager.hpp"

ShaderManager::ShaderManager()
{
	_loadShaders();
}

ShaderManager::~ShaderManager()
{
}

void ShaderManager::_loadShaders()
{
	std::string buttonSrc = "shaders/button";
	_button.initShaders(const_cast<char*>(buttonSrc.c_str()), true, true, false);

	std::string spaceshipSrc = "shaders/spaceship";
	_spaceshipShader.initShaders(const_cast<char*>(spaceshipSrc.c_str()), true, true, false);

	std::string enemySrc = "shaders/enemy";
	_enemyShader.initShaders(const_cast<char*>(enemySrc.c_str()), true, true, false);
	_explodingEnemy.initShaders(const_cast<char*>(enemySrc.c_str()), true, true, true);

	std::string lightSourceSrc = "shaders/lightsource";
	_lightSourceShader.initShaders(const_cast<char*>(lightSourceSrc.c_str()), true, true, false);

	std::string bulletSrc = "shaders/bullet";
	_bulletShader.initShaders(const_cast<char*>(bulletSrc.c_str()), true, true, false);

	std::string laserSrc = "shaders/laser";
	_laserShader.initShaders(const_cast<char*>(laserSrc.c_str()), true, true, false);

	std::string textSrc = "shaders/text";
	_textShader.initShaders(const_cast<char*>(textSrc.c_str()), true, true, false);
}
