#include "../include/GameManager.hpp"

#include "glm/gtc/type_ptr.hpp"
#include <random>
#include <iostream>

GameManager::GameManager(Timer* timer) : _timer(timer)
{
}

GameManager::~GameManager()
{
}

void GameManager::privateInit()
{
	_text.init();

	_basicGeom = new BasicGeometry();
	_textureManager = new TextureManager();
	_shaderManager = new ShaderManager();
	_modelsProvider = new ModelProvider();

	setProjectionMatrix(glm::perspective(glm::radians(60.0f), 1400.0f / 700.0f, 1.0f, 3000.0f));

	//Camera
	_cam.reset(new Camera());
	_viewMatrix = _cam->viewMatrix();

	// Upgrade buttons
	_buttons.push_back(std::make_shared<Button>(this, 0.9f, 0.44f, 0.12f, 0.11f, "bulletDamage"));
	_buttons.push_back(std::make_shared<Button>(this, 0.9f, 0.32f, 0.12f, 0.11f, "bulletSpeed"));
	_buttons.push_back(std::make_shared<Button>(this, 0.9f, 0.20f, 0.12f, 0.11f, "laserDamage"));
	_buttons.push_back(std::make_shared<Button>(this, 0.9f, 0.08f, 0.12f, 0.11f, "laserWidth"));
	_buttons.push_back(std::make_shared<Button>(this, 0.9f, -0.04f, 0.12f, 0.11f, "spaceshipHealth"));
	_buttons.push_back(std::make_shared<Button>(this, 0.9f, -0.16f, 0.12f, 0.11f, "spaceshipSpeed"));

	_skybox.reset(new Skybox(this));
	_skybox->init();

	_bf.reset(new BattleField(this));
	this->addSubObject(_bf);

	_water.reset(new Water(this));
	this->addSubObject(_water);

	_spaceship.reset(new SpaceShip(this));
	this->addSubObject(_spaceship);

	_light.reset(new DirectionalLight(this, glm::vec3(0.0f, 200.0f, 200.0f), glm::vec3(1.0f, 1.0f, 1.0f)));
	this->addSubObject(_light);
}

// Fire laser from spaceship
void GameManager::fireLaser() {
	if (_spaceship->canFireLaser()) {
		auto projectile = std::make_shared<Laser>(this, _spaceship->getPos(), _spaceship->laserDamage(), _spaceship->laserWidth());
		_lasers.insert(projectile);
		addSubObject(projectile);
		_spaceship->fireLaser();
	}
}

// Fire bullet from spaceship
void GameManager::fireBullet() {
	if (_spaceship->canFireBullet()) {
		auto bullet = std::make_shared<Bullet>(this, _spaceship->getPos(), glm::vec3(0.0f, 0.0f, 1.0f), true, _spaceship->bulletDamage(), _spaceship->bulletSpeed());
		_bullets.insert(bullet);
		addSubObject(bullet);
		_spaceship->fireBullet();
	}
}

void GameManager::privateRender()
{
	_skybox->render();
	
	// Draw text.
	auto shader = _shaderManager->textShader();
	shader->enable();

	char buffer[80];

	// Health text
	shader->setUniform3fv("color", glm::value_ptr(glm::vec3(1.0f, 0.0f, 0.0f)));
	sprintf(buffer, "Health: %.0f", _spaceship->life());
	_text.setPos(-0.95f, -0.95f, 0.0f);
	_text.printString(buffer, Text::FONT_NORMAL);

	// Gold text
	shader->setUniform3fv("color", glm::value_ptr(glm::vec3(0.0f, 0.0f, 0.0f)));
	sprintf(buffer, "Gold: %d", _gold);
	_text.setPos(0.85f, 0.55f, 0.0f);
	_text.printString(buffer, Text::FONT_NORMAL);

	// Upgrade text for all buttons.
	sprintf(buffer, "Upgrades:");
	_text.setPos(0.85f, 0.5f, 0.0f);
	_text.printString(buffer, Text::FONT_NORMAL);

	sprintf(buffer, "Bullet");
	_text.setPos(0.85f, 0.45f, 0.0f);
	_text.printString(buffer, Text::FONT_SMALL);

	sprintf(buffer, "Damage +%.02f", _spaceship->bulletDamageUpgradeAmount());
	_text.setPos(0.85f, 0.425f, 0.0f);
	_text.printString(buffer, Text::FONT_SMALL);

	sprintf(buffer, "Cost: %d", _spaceship->bulletDamageUpgradeCost());
	_text.setPos(0.85f, 0.4f, 0.0f);
	_text.printString(buffer, Text::FONT_SMALL);

	sprintf(buffer, "Bullet");
	_text.setPos(0.85f, 0.33f, 0.0f);
	_text.printString(buffer, Text::FONT_SMALL);

	sprintf(buffer, "Speed +%.02f", _spaceship->bulletSpeedUpgradeAmount());
	_text.setPos(0.85f, 0.305f, 0.0f);
	_text.printString(buffer, Text::FONT_SMALL);

	sprintf(buffer, "Cost: %d", _spaceship->bulletSpeedUpgradeCost());
	_text.setPos(0.85f, 0.28f, 0.0f);
	_text.printString(buffer, Text::FONT_SMALL);

	sprintf(buffer, "Laser");
	_text.setPos(0.85f, 0.21f, 0.0f);
	_text.printString(buffer, Text::FONT_SMALL);

	sprintf(buffer, "Damage +%0.2f", _spaceship->laserDamageUpgradeAmount());
	_text.setPos(0.85f, 0.185f, 0.0f);
	_text.printString(buffer, Text::FONT_SMALL);

	sprintf(buffer, "Cost: %d", _spaceship->laserDamageUpgradeCost());
	_text.setPos(0.85f, 0.16f, 0.0f);
	_text.printString(buffer, Text::FONT_SMALL);

	sprintf(buffer, "Laser");
	_text.setPos(0.85f, 0.09f, 0.0f);
	_text.printString(buffer, Text::FONT_SMALL);

	sprintf(buffer, "Width +%.02f", _spaceship->laserWidthUpgradeAmount());
	_text.setPos(0.85f, 0.065f, 0.0f);
	_text.printString(buffer, Text::FONT_SMALL);

	sprintf(buffer, "Cost: %d", _spaceship->laserWidthUpgradeCost());
	_text.setPos(0.85f, 0.04f, 0.0f);
	_text.printString(buffer, Text::FONT_SMALL);

	sprintf(buffer, "Spaceship");
	_text.setPos(0.85f, -0.03f, 0.0f);
	_text.printString(buffer, Text::FONT_SMALL);

	sprintf(buffer, "Health +%.02f", _spaceship->lifeUpgradeAmount());
	_text.setPos(0.85f, 0.-0.055f, 0.0f);
	_text.printString(buffer, Text::FONT_SMALL);

	sprintf(buffer, "Cost: %d", _spaceship->spaceshipLifeUpgradeCost());
	_text.setPos(0.85f, -0.08f, 0.0f);
	_text.printString(buffer, Text::FONT_SMALL);

	sprintf(buffer, "Spaceship");
	_text.setPos(0.85f, -0.15f, 0.0f);
	_text.printString(buffer, Text::FONT_SMALL);

	sprintf(buffer, "Speed +%.02f", _spaceship->speedUpgradeAmount());
	_text.setPos(0.85f, -0.175f, 0.0f);
	_text.printString(buffer, Text::FONT_SMALL);

	sprintf(buffer, "Cost: %d", _spaceship->spaceshipSpeedUpgradeCost());
	_text.setPos(0.85f, -0.2f, 0.0f);
	_text.printString(buffer, Text::FONT_SMALL);

	// Score text
	sprintf(buffer, "Score: %d", _score);
	_text.setPos(-0.95f, 0.90f, 0.0f);
	_text.printString(buffer, Text::FONT_NORMAL);

	// Instructions text
	sprintf(buffer, "A and D to move, K = laser, L = bullets");
	_text.setPos(-0.25f, -0.95f, 0.0f);
	_text.printString(buffer, Text::FONT_NORMAL);

	// Fps text
	sprintf(buffer, "fps: %.0f", _fps);
	_text.setPos(0.85f, 0.90f, 0.0f);
	_text.printString(buffer, Text::FONT_NORMAL);

	glFlush();

	shader->disable();

	// Render buttons
	for (std::shared_ptr<Button> button : _buttons) {
		(*button).render();
	}
}

void GameManager::privateUpdate()
{
	// Check if a new enemy can be spawned
	_lastEnemyTime += static_cast<float>(_dt);
	if (_lastEnemyTime > _enemyCdTime) {
		std::mt19937 rng;
		rng.seed(std::random_device()());
		std::uniform_int_distribution<std::mt19937::result_type> strafeWidthGen(30, 60);
		int strafeWidth = strafeWidthGen(rng);
		std::uniform_int_distribution<std::mt19937::result_type> xPosGen(0, (int)_bf->width() - 2 * strafeWidth);
		std::uniform_int_distribution<std::mt19937::result_type> healthGen(1, _enemyMaxHealth);

		float health = (float)healthGen(rng);
		float pos_x = -((_bf->width() / 2) - strafeWidth) + (xPosGen(rng));
		auto enemy = std::make_shared<Enemy>(this, health, (float)strafeWidth, glm::vec3(pos_x, 0.0f, 500.0f));
		_enemies.insert(enemy);
		this->addSubObject(enemy);
		printf("--Enemy spawned\n");

		_totalEnemiesSpawned++;
		if (_enemyMaxHealth < 6 && _totalEnemiesSpawned / 10 >= _enemyMaxHealth) {
			_enemyMaxHealth++;
		}

		_lastEnemyTime = 0;
		if (_enemyCdTime > 0.7f) {
			_enemyCdTime -= 0.01f;
		}
	}

	// Check if any lasers should be removed
	for (auto li = _lasers.begin(); li != _lasers.end(); ) {
		(*li)->checkForHits();
		if ((*li)->shouldDestroy()) {
			removeSubObject((*li));
			li = _lasers.erase(li);
		}
		else {
			++li;
		}
	}

	// Check if any bullets should be removed
	for (auto bi = _bullets.begin(); bi != _bullets.end(); ) {
		if ((*bi)->shouldDestroy() || (*bi)->checkForHits()) {
			removeSubObject((*bi));
			bi = _bullets.erase(bi);
		}
		else {
			++bi;
		}
	}

	for (auto ei = _enemies.begin(); ei != _enemies.end(); ) {
		// Check if enemies are colliding with the player
		if (glm::length(_spaceship->getPos() - (*ei)->getPos()) < _spaceship->radius() + (*ei)->radius()) {
			_spaceship->takeDamage((*ei)->health());
			removeSubObject((*ei));
			ei = _enemies.erase(ei);
			printf("--Enemy Collided with player\n");
		}
		// Remove enemy if it is behind the player
		else if ((*ei)->getPos().z < -100) {
			_spaceship->takeDamage((*ei)->health());
			removeSubObject((*ei));
			ei = _enemies.erase(ei);
			printf("--Enemy went below the player\n");
		}
		// Check if enemy can shoot, if yes fire a bullet.
		else if ((*ei)->canShoot()) {
			auto bullet = std::make_shared<Bullet>(this, (*ei)->getPos(), glm::vec3(0.0f, 0.0f, -1.0f), false, 1.0f, 200.0f);
			_bullets.insert(bullet);
			addSubObject(bullet);
			(*ei)->fireBullet();
		}
		else {
			++ei;
		}
	}

	// Check if any exploding enemy animations should be removed
	for (auto exi = _explodingEnemies.begin(); exi != _explodingEnemies.end();) {
		if ((*exi)->shouldRemove()) {
			removeSubObject((*exi));
			exi = _explodingEnemies.erase(exi);
		}
		else {
			++exi;
		}
	}
}

void GameManager::removeEnemy(std::shared_ptr<Enemy> enemy) {
	removeSubObject(enemy);
	_enemies.erase(_enemies.find(enemy));
}

void GameManager::keyDown(unsigned char key, int x, int y)
{
}

void GameManager::keyUp(unsigned char key, int x, int y)
{
	_cam->keyUp(key, x, y);
}

void GameManager::mousePressed(float x, float y)
{
	for (auto button : _buttons) {
		button->mousePressed(x, y);
	}
}

void GameManager::moveLeft() {
	float amount = static_cast<float>(_spaceship->speed() * _dt);
	if (!_cam->isMovingRight() && _spaceship->posX() + amount <= (_bf->width() / 2 - _spaceship->radius())) {
		_spaceship->moveLeft(amount);
		_cam->moveLeft(amount);
	}
	_viewMatrix = _cam->viewMatrix();
}

void GameManager::moveRight() {
	float amount = static_cast<float>(_spaceship->speed() * _dt);
	if (!_cam->isMovingLeft() && _spaceship->posX() - amount >= (-_bf->width() / 2 + _spaceship->radius())) {
		_spaceship->moveRight(-amount);
		_cam->moveRight(-amount);
	}
	_viewMatrix = _cam->viewMatrix();
}

// Function that deals with callbacks from upgrade buttons.
void GameManager::buttonCallback(std::string type)
{
	if (type == "bulletDamage") {
		_gold -= _spaceship->upgradeBulletDamage(_gold);
	}
	else if (type == "bulletSpeed") {
		_gold -= _spaceship->upgradeBulletSpeed(_gold);
	}
	else if (type == "laserDamage") {
		_gold -= _spaceship->upgradeLaserDamage(_gold);
	}
	else if (type == "laserWidth") {
		_gold -= _spaceship->upgradeLaserWidth(_gold);
	}
	else if (type == "spaceshipHealth") {
		_gold -= _spaceship->upgradeHealth(_gold);
	}
	else if (type == "spaceshipSpeed") {
		_gold -= _spaceship->upgradeSpeed(_gold);
	}
	else {
		std::cout << "--unknown button callback" << std::endl;
	}
}

void GameManager::addExplodingEnemy(glm::mat4 enemyMatrix, float size, float radius)
{
	auto explodingEnemy = std::make_shared<ExplodingEnemy>(this, enemyMatrix, size, radius);
	_explodingEnemies.insert(explodingEnemy);
	addSubObject(explodingEnemy);
}
