#include "../include/BasicGeometry.hpp"

#include "glm/gtc/type_ptr.hpp"

#include <vector>

BasicGeometry::BasicGeometry()
{
	_privateInit();
}

BasicGeometry::~BasicGeometry()
{
	glDeleteVertexArrays(1, &_cubeVAO);
	glDeleteVertexArrays(1, &_texturedQuadVAO);
}

void BasicGeometry::_privateInit()
{
	// Cube
	float halfWidth = 0.5f;
	std::vector<glm::vec3> cubeVertices = {
		// LEFT
		glm::vec3(-halfWidth, -halfWidth, halfWidth),
		glm::vec3(-halfWidth, -halfWidth, -halfWidth),
		glm::vec3(-halfWidth, halfWidth, -halfWidth),
		glm::vec3(-halfWidth, halfWidth, halfWidth),

		// RIGHT
		glm::vec3(halfWidth, -halfWidth, -halfWidth),
		glm::vec3(halfWidth, -halfWidth, halfWidth),
		glm::vec3(halfWidth, halfWidth, halfWidth),
		glm::vec3(halfWidth, halfWidth, -halfWidth),

		// FRONT
		glm::vec3(-halfWidth, -halfWidth, -halfWidth),
		glm::vec3(halfWidth, -halfWidth, -halfWidth),
		glm::vec3(halfWidth, halfWidth, -halfWidth),
		glm::vec3(-halfWidth, halfWidth, -halfWidth),

		// BACK
		glm::vec3(halfWidth, -halfWidth, halfWidth),
		glm::vec3(-halfWidth, -halfWidth, halfWidth),
		glm::vec3(-halfWidth, halfWidth, halfWidth),
		glm::vec3(halfWidth, halfWidth, halfWidth),

		// UP
		glm::vec3(-halfWidth, halfWidth, halfWidth),
		glm::vec3(-halfWidth, halfWidth, -halfWidth),
		glm::vec3(halfWidth, halfWidth, -halfWidth),
		glm::vec3(halfWidth, halfWidth, halfWidth),

		// DOWN
		glm::vec3(-halfWidth, -halfWidth, -halfWidth),
		glm::vec3(-halfWidth, -halfWidth, halfWidth),
		glm::vec3(halfWidth, -halfWidth, halfWidth),
		glm::vec3(halfWidth, -halfWidth, -halfWidth)
	};
	_numCubeVertices = cubeVertices.size();

	std::vector<glm::vec3> cubeNormals = {
		// LEFT
		glm::vec3(-1.0f, 0.0f, 0.0f),
		glm::vec3(-1.0f, 0.0f, 0.0f),
		glm::vec3(-1.0f, 0.0f, 0.0f),
		glm::vec3(-1.0f, 0.0f, 0.0f),

		// RIGHT
		glm::vec3(1.0f, 0.0f, 0.0f),
		glm::vec3(1.0f, 0.0f, 0.0f),
		glm::vec3(1.0f, 0.0f, 0.0f),
		glm::vec3(1.0f, 0.0f, 0.0f),

		// FRONT
		glm::vec3(0.0f, 0.0f, -1.0f),
		glm::vec3(0.0f, 0.0f, -1.0f),
		glm::vec3(0.0f, 0.0f, -1.0f),
		glm::vec3(0.0f, 0.0f, -1.0f),

		// BACK
		glm::vec3(0.0f, 0.0f, 1.0f),
		glm::vec3(0.0f, 0.0f, 1.0f),
		glm::vec3(0.0f, 0.0f, 1.0f),
		glm::vec3(0.0f, 0.0f, 1.0f),

		// UP
		glm::vec3(0.0f, 1.0f, 0.0f),
		glm::vec3(0.0f, 1.0f, 0.0f),
		glm::vec3(0.0f, 1.0f, 0.0f),
		glm::vec3(0.0f, 1.0f, 0.0f),

		// DOWN
		glm::vec3(0.0f, -1.0f, 0.0f),
		glm::vec3(0.0f, -1.0f, 0.0f),
		glm::vec3(0.0f, -1.0f, 0.0f),
		glm::vec3(0.0f, -1.0f, 0.0f)
	};

	GLuint cubeVBO, cubeNBO;
	glGenVertexArrays(1, &_cubeVAO);
	glGenBuffers(1, &cubeVBO);
	glGenBuffers(1, &cubeNBO);
	glBindVertexArray(_cubeVAO);

	glBindBuffer(GL_ARRAY_BUFFER, cubeVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * cubeVertices.size(), &cubeVertices[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (void*)0);

	glBindBuffer(GL_ARRAY_BUFFER, cubeNBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * cubeNormals.size(), &cubeNormals[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (void*)0);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Textured Quad
	std::vector<glm::vec3> quadVertices = {
		glm::vec3(-halfWidth, -halfWidth, 0),
		glm::vec3(halfWidth, -halfWidth, 0),
		glm::vec3(halfWidth, halfWidth, 0),
		glm::vec3(-halfWidth, halfWidth, 0)
	};
	_numTexturedQuadVertices = quadVertices.size();

	std::vector<glm::vec2> quadTexCoords = {
		glm::vec2(0.0f, 0.0f),
		glm::vec2(1.0f, 0.0f),
		glm::vec2(1.0f, 1.0f),
		glm::vec2(0.0f, 1.0f)
	};

	GLuint quadVBO, quadTBO;
	glGenVertexArrays(1, &_texturedQuadVAO);
	glGenBuffers(1, &quadVBO);
	glGenBuffers(1, &quadTBO);
	glBindVertexArray(_texturedQuadVAO);

	glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * quadVertices.size(), &quadVertices[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (void*)0);

	glBindBuffer(GL_ARRAY_BUFFER, quadTBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * quadTexCoords.size(), &quadTexCoords[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), (void*)0);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}