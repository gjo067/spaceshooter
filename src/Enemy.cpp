#include "../include/Enemy.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "../include/GameManager.hpp"


Enemy::Enemy(GameManager* gm, float health, float strafeWidth, glm::vec3 pos)
	: _gm(gm), _health(health), _strafeWidth(strafeWidth), _strafeCounter(0), _startHealth(health)
{
	_size = 0.2f + 0.15f * _health;
	_radius = 35.0f * _size;
	_speed = (1 / std::ceil(_health)) * 75.0f;

	setPosition(pos);
}

Enemy::~Enemy()
{
}

void Enemy::setPosition(glm::vec3 pos)
{
	_modelMatrix[3] = glm::vec4(pos, _modelMatrix[3][3]);
}

// returns true if the damage is lethal.
bool Enemy::takeDamage(float damage)
{
	_health -= damage;

	if (_health <= 0.0f)
		return true;
	
	_size = 0.2f + 0.15f * _health;
	_radius = 35.0f * _size;
	_speed = (1 / std::ceil(_health)) * 75.0f;

	return false;
}

void Enemy::privateInit()
{
}

glm::vec3 Enemy::_getColorFromHealth() {
	glm::vec3 color = glm::vec3(0.0f, 0.0f, 0.0f);
	if (_health <= 1.0f) {
		color = glm::vec3(1.0f, 0.0f, 0.0f);
	}
	else if (_health <= 2.0f) {
		color = glm::vec3(1.0f, 0.6f, 0.0f);
	}
	else if (_health <= 3.0f) {
		color = glm::vec3(1.0f, 1.0f, 0.0f);
	}
	else if (_health <= 4.0f) {
		color = glm::vec3(0.4f, 1.0f, 0.2f);
	}
	else if (_health <= 5.0f) {
		color = glm::vec3(1.0f, 1.0f, 1.0f);
	}
	return color;
}

void Enemy::privateRender()
{
	_strafeCounter += static_cast<float>(_dt * 5.0f);
	_modelMatrix = glm::translate(_modelMatrix, glm::vec3(std::sin(_strafeCounter) * _strafeWidth * _dt, 0.0f, -_dt * _speed));

	glm::mat4 model = _modelMatrix;
	model = glm::translate(model, glm::vec3(0.0f, -_radius * 0.67f, 0.0f));
	//model = glm::rotate(model, (float)glm::radians(_dt * 10.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(_size, _size, _size));

	Shader* shader = _gm->shaders()->enemyShader();
	shader->enable();

	shader->setUniform1f("size", _size);
	shader->setUniform3fv("lightColor", glm::value_ptr(_gm->light()->color()));
	shader->setUniform3fv("objectColor", glm::value_ptr(_getColorFromHealth()));
	shader->setUniform3fv("lightPos", glm::value_ptr(_gm->light()->pos()));
	shader->setUniformMat4fv("model", glm::value_ptr(model));
	shader->setUniformMat4fv("projection", glm::value_ptr(_gm->projectionMatrix()));
	shader->setUniformMat4fv("view", glm::value_ptr(_gm->viewMatrix()));

	_gm->models()->enemy()->draw(shader);

	shader->disable();
}

void Enemy::privateUpdate()
{
	if (!_canFireBullet) {
		_bulletCdTime += _dt;
		if (_bulletCdTime >= _bulletCd) {
			_bulletCdTime = 0;
			_canFireBullet = true;
		}
	}
}