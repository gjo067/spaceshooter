#include "..\include\Water.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "../include/GameManager.hpp"

#include <iostream>

Water::Water(GameManager* gm) : _gm(gm)
{
}

Water::~Water()
{
	glDeleteVertexArrays(1, &_VAO);
}

void Water::privateInit()
{
	int height = 256;
	int width = 256;

	_modelMatrix = glm::translate(_modelMatrix, glm::vec3(-(width * _size) / 2.0f, -70.0f, 0.0f));
	_modelMatrix = glm::scale(_modelMatrix, glm::vec3(1.0f, 1.0f, 3.0f));

	std::vector<glm::vec3> vertices;
	std::vector<GLuint> indices;

	for (int i = 0; i < height; ++i) {
		for (int j = 0; j < width; ++j) {
			vertices.push_back(glm::vec3(j * _size, 0.0f, i * _size));
		}
	}

	for (int i = 0; i < height - 1; ++i) { // i = [0,511]
		for (int j = 0; j < width; ++j) { // j = [0,31]
			// 0, 32, 1, 33, 2, 34, 3, 35, ... , 31, 63, 63, 32
			// 32, 64, 33, 65, 34, 66, 35, 67, ... , 63, 95, 95, 64
			indices.push_back(j + i * width);
			indices.push_back(j + width + i * width);
		}
		if (i < height - 1) {
			indices.push_back((i + 2) * width - 1);
			indices.push_back((i + 1) * width);
		}
	}

	_numIndices = indices.size();

	GLuint VBO;
	glGenVertexArrays(1, &_VAO);
	glGenBuffers(1, &_EBO);
	glGenBuffers(1, &VBO);
	glBindVertexArray(_VAO);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * indices.size(), &indices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * vertices.size(), &vertices[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (void*)0);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	std::string name = "shaders/water";
	_shader.initShaders(const_cast<char*>(name.c_str()), true, true, false);
}

void Water::privateRender()
{
	_shader.enable();

	_shader.setUniformMat4fv("model", glm::value_ptr(_modelMatrix));
	_shader.setUniformMat4fv("view", glm::value_ptr(_gm->viewMatrix()));
	_shader.setUniformMat4fv("projection", glm::value_ptr(_gm->projectionMatrix()));
	_shader.setUniform3fv("eyePos", glm::value_ptr(_gm->cam()->pos()));
	_shader.setUniform1f("time", (float)_gm->timer()->elapsed());
	_shader.setUniform1f("size", _size);
	_shader.setUniform1i("texSky", 8);

	glActiveTexture(GL_TEXTURE8);
	glBindTexture(GL_TEXTURE_CUBE_MAP, _gm->skybox()->texture());

	glBindVertexArray(_VAO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _EBO);
	glDrawElements(GL_TRIANGLE_STRIP, _numIndices, GL_UNSIGNED_INT, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	_shader.disable();
}

void Water::privateUpdate()
{
}
